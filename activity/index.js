console.log('Hello World')

// Addition and Subtraction without return
function addition (num1, num2){
	console.log('Displayed sum of ' + num1 + ' and ' + num2);
	console.log(num1 + num2)
}
addition(5,15);

function subtraction (num1, num2){
	console.log('Displayed difference of ' + num1 + ' and ' + num2);
	console.log(num1 - num2);
}
subtraction(20,5);

// Multiplication and Division with return
function multiplication(num1, num2){
	console.log('The product of ' + num1 + ' and ' + num2);
	return num1*num2;
}
let product = multiplication(50,10);
console.log(product);

function division(num1, num2){
	console.log('The quotient of ' + num1 + ' and ' + num2);
	return num1/num2;
}
let quotient = division(50,10);
console.log(product);

// Area of a circle
function getAreaOfCircle(radius) {
	let pi = 3.14159
	console.log('The result of getting the area of a circle with ' + radius + ' radius:');
	return pi*radius*radius;
}
let circleArea = getAreaOfCircle(15);
console.log(circleArea);

// Average of four numbers
function getAverage(num1, num2, num3, num4){
	console.log('The average of ' + num1 + ', ' + num2 + ', ' + num3 + ', ' + num4)
	return (num1+num2+num3+num4)/4
}
let averageVar = getAverage(20,40,60,80);
console.log(averageVar);

// Percentage
function getPercentage(num1, num2){
	console.log('Is ' + num1 + '/' + num2 + ' a passing score?')
	let percent = (num1/num2)*100;
	let isPassed = percent >= 75;
	return isPassed;
}
let isPassingScore = getPercentage(38,50);
console.log(isPassingScore);