// Function able to receive data without the use of global variables or prompt();

// "name" - is a parameter
// A parameter is a variable/container that exists only in our function and is used to store information that is provided to a function when it is called/invoke. 
function printName(name){
	console.log("My name is " + name);
};

// Data passed into a function can be received by the function.
// This is what we call an argument.
printName("Juana");
printName("Jimin");

function printMyAge(age){
	console.log("I am " + age);
};

printMyAge(25);
printMyAge();

function checkDivisibilityBy8(num){
	let remainder = num % 8;
	console.log("The remainder of " + num + " divided by 8 is: " + remainder);
	let isDivisibleBy8 = remainder === 0; 
	console.log("Is " + num + " divisible by 8?")
	console.log(isDivisibleBy8);
}
checkDivisibilityBy8(64);
checkDivisibilityBy8(27);

function printMFavoriteSuperhero(hero){
	console.log("My favorite superhero is " + hero);
};

printMFavoriteSuperhero('Deku');

// Debug

function printMyfavoritelanguage(language){
	console.log("My favorite language is " + language);
};

printMyfavoritelanguage("Javasript");
printMyfavoritelanguage("Java");

// Multiple arguments can also be passed into a function. Multiple paramenters can contain our arguments.

function printFullName(firstName, middleName, lastName){
	console.log(firstName + " " + middleName + " " + lastName);
};

printFullName("Juan", "Crisostomo", "Ibarra");
printFullName("Ibarra", "Crisostomo", "Juan");
printFullName("Dahyun", "Kim", "Dela Cruz");

/*
	Parameters will contain the arguments according to the order it was passed
*/

// In other languages, providing more/less arguments that the expected parameters sometimes causes an error or changes the behaviour of the function.

// In Javascript, we don't have to worry about that.

// In javascript, less arguments returns undefined parameters. 

printFullName("Stephen", "Wardell");
printFullName("Stephen", "Wardell", "Curry");
// commas are used to separate values.
printFullName("Stephen", "Wardell", "Curry", "James");

// Use Variable as Arguments
let fName = "Larry";
let mName = "Joe";
let lName = "Bird";

printFullName(fName, mName, lName)

// Mini Activity
function printFavoriteSong (song1, song2, song3, song4, song5){
	let listSong = [song1, song2, song3, song4, song5];
	console.log('My favorite songs are')
	console.log(listSong)

	return "pogi ko";
};

printFavoriteSong('ABC', 'Twinkle Little Star', 'Bahay Kubo', 'BaBaBlack Sheep', 'Baby Shark');

// Return Statement

// Currently or so far, our functins are able to display dta in our console.
// However, our functions can't return a values yet. 

let fullName = printFullName('Mark', 'Joseph', 'Lacdao');
console.log(fullName);

function returnFullName(firstName, middleName, lastName ) {
	return firstName + ' ' + middleName + ' ' + lastName;
};

fullName = returnFullName('Ernesto', 'Antonio', 'Maceda');
console.log(fullName);

function returPhilippineAddress(city){
	return city + ',Philippines';
}
let myFullAddress = returPhilippineAddress('Gensan');
console.log(myFullAddress);

function checkDivisibilityBy4(num){
	let remainder = num % 4;
	let isDivisibleBy4 = remainder === 0;

	return isDivisibleBy4;
	// return also ends the process of the function
	console.log('checking');
}

let num4isDivisibleBy4 = checkDivisibilityBy4(40);
let num14isDivisibleBy4 = checkDivisibilityBy4(14);

console.log(num4isDivisibleBy4);
console.log(num14isDivisibleBy4);

// Mini Activity
function createPlayerInfo(username, level, job){
	return 'username: ' + username + ' ' + ', level: ' + level + ', Job: ' + job;
}
let user1 = createPlayerInfo("white_night",95, 'Paladin');
console.log(user1);